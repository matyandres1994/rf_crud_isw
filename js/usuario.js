function checkRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.', '');
    // Despejar Guión
    valor = valor.replace('-', '');
    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    // Formatear RUN
    rut.value = cuerpo + '-' + dv
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if (cuerpo.length < 7) {
        rut.setCustomValidity("RUT Incompleto");
        return false;
    }
    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;
    // Para cada dígito del Cuerpo
    for (i = 1; i <= cuerpo.length; i++) {
        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);
        // Sumar al Contador General
        suma = suma + index;
        // Consolidar Múltiplo dentro del rango [2,7]
        if (multiplo < 7) {
            multiplo = multiplo + 1;
        } else {
            multiplo = 2;
        }
    }
    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);
    // Casos Especiales (0 y K)
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    // Validar que el Cuerpo coincide con su Dígito Verificador
    if (dvEsperado != dv) {
        rut.setCustomValidity("RUT Inválido");
        return false;
    }
    // Si todo sale bien, eliminar errores (decretar que es válido)
    rut.setCustomValidity('');
}

function validaRut(Objeto) {
    var tmpstr = "";
    var intlargo = Objeto.value
    if (intlargo.length > 0) {
        crut = Objeto.value
        largo = crut.length;
        if (largo < 2) {
            toastr.error('El rut ingresado es invalido');
            Objeto.value = "";
            return false;
        }
        for (i = 0; i < crut.length; i++)
            if (crut.charAt(i) != ' ' && crut.charAt(i) != '.' && crut.charAt(i) != '-') {
                tmpstr = tmpstr + crut.charAt(i);
            }
        rut = tmpstr;
        crut = tmpstr;
        largo = crut.length;
        if (largo > 2) rut = crut.substring(0, largo - 1);
        else rut = crut.charAt(0);
        dv = crut.charAt(largo - 1);
        if (rut == null || dv == null) return 0;
        var dvr = '0';
        suma = 0;
        mul = 2;
        for (i = rut.length - 1; i >= 0; i--) {
            suma = suma + rut.charAt(i) * mul;
            if (mul == 7) mul = 2;
            else mul++;
        }
        res = suma % 11;
        if (res == 1) dvr = 'k';
        else if (res == 0) dvr = '0';
        else {
            dvi = 11 - res;
            dvr = dvi + "";
        }
        if (dvr != dv.toLowerCase()) {
            toastr.error('El rut ingresado es invalido');
            // Objeto.style.backgroundColor = '#F18C8C';
            Objeto.value = "";
            Objeto.focus();
            return false;
        }
        //Objeto.style.backgroundColor = '#6FD84C';
        Objeto.focus()
        return true;
    }
}
$(document).ready(function() {
    cargarTabla();
    $("#usuario").inputmask({
        mask: "9[9.999.99]-[9|K|k]",
    });

    function generar(longitud) {
        long = parseInt(longitud);
        var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ2346789";
        var contrasena = "";
        for (i = 0; i < long; i++) contrasena += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
        return contrasena;
    }

    function cargarTabla() {
        var codigo = $('#idequipo').val();
        $.ajax({
            type: "GET",
            url: "../php/cargarUsuarios.php",
            success: function(response) {
                var datos1 = JSON.parse(response);
                if (datos1.respuesta != "ok") {
                    alert();
                } else {
                    $("#tablaUsuario").html("");
                    $("#tablaUsuario").html(datos1.resultadohtml);
                    $(".btnEliminar").click(function() {
                        $("#modalEliminar").modal("show");
                        var nombre = $(this).parents("tr").find("td")[2].innerHTML;
                        var codigo = $(this).parents("tr").find("td")[0].innerHTML;
                        $("#rutUsuario").val(codigo);
                        $("#nombre").text(nombre);
                        $(".btnBorrar1").click(function() {
                            var codigo1 = $("#rutUsuario").val();
                            $.ajax({
                                type: "GET",
                                url: "../php/eliminarUsuarios.php",
                                data: {
                                    'codigo': codigo
                                },
                                success: function(response) {
                                    $("#tablaUsuario").html("");
                                    $("#modalEliminar").modal("hide");
                                    cargarTabla();
                                    toastr.remove();
                                    toastr.info("Se ha eliminado exitosamente");
                                },
                                failure: function(response) {}
                            });
                        });
                    });
                    $(".btnGenerar").click(function() {
                        var pass = generar(8);
                        var codigo = $(this).parents("tr").find("td")[0].innerHTML;
                        $.ajax({
                            type: "GET",
                            url: "../php/actualizarPass.php",
                            data: {
                                'rut': codigo,
                                'password': pass
                            },
                            success: function(response) {
                                $("#tablaUsuario").html("");
                                cargarTabla();
                                toastr.remove();
                                toastr.info("Se genero contraseña");
                            },
                            failure: function(response) {}
                        });
                    });
                    $(".btnEditar").click(function() {
                        var codigo = $(this).parents("tr").find("td")[0].innerHTML;
                        var correo = $(this).parents("tr").find("td")[1].innerHTML;
                        var nombre = $(this).parents("tr").find("td")[2].innerHTML;
                        var contrasena = $(this).parents("tr").find("td")[3].innerHTML;
                        var Sucursal = $(this).parents("tr").find("td")[4].innerHTML;
                        var rol = $(this).parents("tr").find("td")[5].innerHTML;
                        $("#rut1").val(codigo);
                        $("#correo1").val(correo);
                        $("#nombre1").val(nombre);
                        $("#contrasena12").val(contrasena);
                        $("#rol1").val(rol);
                        $("#codigo1").val(Sucursal);
                        $("#ModalEdita").modal("show");
                        $(".btnActualizar").click(function() {
                            var ruta = $("#rut1").val();
                            var correoa = $("#correo1").val();
                            var nombrea = $("#nombre1").val();
                            var passa = $("#contrasena12").val();
                            var rola = $("#rol1").val();
                            var codigoa = $("#codigo1").val();
                            $.ajax({
                                type: "GET",
                                url: "../php/actualizarUsuarios.php",
                                data: {
                                    'rut': ruta,
                                    'correo': correoa,
                                    'nombre': nombrea,
                                    'password': passa,
                                    'rol': rola,
                                    'id': codigoa
                                },
                                success: function(response) {
                                    $("#tablaUsuario").html("");
                                    $("#ModalEdita").modal("hide");
                                    cargarTabla();
                                    toastr.remove();
                                    toastr.info("los datos se han actualizado exitosamente");
                                },
                                failure: function(response) {}
                            });
                        });
                    });
                }
            },
            failure: function(response) {
                alert();
            }
        });
    }
});