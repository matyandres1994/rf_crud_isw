<?php
include 'conexion.php';

$respuesta     = "";
$resultadohtml = "";

$query = "select * from usuario";
$datos = mysqli_query($conn, $query);
$resultadohtml .= "<table id='tablaUsuario' class='table table-responsive col-lg-12'";
$resultadohtml .= "<tr style='text-align: center; color:#307BCE ''><td >Rut</td><td>Correo</td><td>Nombre</td><td>Contraseña</td><td>Sucursal</td><td>Rol</td><td>Editar</td><td>Eliminar</td><td>Gen contraseña";
$resultadohtml .= "<caption>Lista de usuarios</caption>";
while ($fila = mysqli_fetch_array($datos)) {
    $resultadohtml .= "<tr style='text-align: center;color:#303030' data-id=" . $fila["rutUsuario"] . "><td>" . $fila["rutUsuario"] . "</td>";
    $resultadohtml .= "<td>" . $fila["correoUsuario"] . "</td>";
    $resultadohtml .= "<td>" . $fila["nombreUsuario"] . "</td>";
    $resultadohtml .= "<td>" . $fila["contrasenaUsuario"] . "</td>";
    $resultadohtml .= "<td>" . $fila["idSucursal"] . "</td>";
    $resultadohtml .= "<td>" . $fila["ROL"] . "</td>";
    
        $resultadohtml .= " <td><a class='btnEditar btn btn-outline-warning'>Editar</a></td>";
        $resultadohtml .= " <td><a class='btnEliminar btn btn-outline-danger'>Eliminar</a></td>";
        $resultadohtml .= " <td><a class='btn btn-outline-success btnGenerar'>Generar pass</a></td>";
    

}
$resultadohtml .= "</table>";

$respuesta = "ok";

mysqli_close($conn);

echo json_encode(array("respuesta" => $respuesta, "resultadohtml" => $resultadohtml));
