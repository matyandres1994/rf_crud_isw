<?php
session_start();
if ($_SESSION['rol'] != 'admin') {
    header("location: ../index.php?sesion=true");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                Administrador
            </title>
            <link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" rel="stylesheet">
            </link>
        </meta>
        <style type="text/css">
            ul{
                background: #FFFDFD;
                padding: 20px;
            }
            ul a{
                font-size: 20px;
            }
            body{
                background: #F1F1F1;
            }

        </style>
    </head>
    <body>
        <header>
            <ul class="nav justify-content-center" id="nav">
                <li class="nav-item">
                    <a class="nav-link active" href="usuario.php">
                        Usuario
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        Asistencia
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../php/archivo.php" target="_blank">
                        Generar informe
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="../index.php">
                        Cerrar sesion
                    </a>
                </li>
            </ul>
        </header>

        <script crossorigin="anonymous" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js">
        </script>
    </body>
</html>
