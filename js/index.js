$(document).ready(function() {
    $("#btnGuardar").click(function() {
        guardar();
    });

    function guardar() {
        var codigo = $('#Codigoedit').val();
        var nombre = $('#descripcionedit').val();
        var email = $('#EmailJugador').val();
        var password = $('#passwordJugador').val();
        var password1 = $('#password1Jugador').val();
        var celular = $('#Celular').val();
        var posicion = $('#posicion').val();
        $.ajax({
            type: "GET",
            url: "~/../php/registrarJugador.php",
            data: {
                'rut': codigo,
                'nombre': nombre,
                'email': email,
                'password': password,
                'password1': password1,
                'celular': celular,
                'posicion': posicion
            },
            datatype: "application/json",
            success: function(response) {
                $('#modalJugador').modal('hide');
                if (response == 1) {
                    location.href = "php/bienvenida.php?registro=si";
                } else {
                    toastr.error('El jugador ya se encuentra registrado');
                }
            },
            error: function(textStatus) {
                alert('no')
            }
        });
    }
});