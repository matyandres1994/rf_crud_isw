<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                Usuarios
            </title>
            <script src="../js/jquery-3.3.1.js">
                </script>
                <script src="../js/bootstrap.min.js">
                </script>
                <link href="../toast/toastr.min.css" rel="stylesheet" type="text/css">
                    <script src="../toast/toastr.min.js" type="text/javascript">
                    </script>
                    <script src="../js/usuario.js"></script>
            <style type="text/css">
                #btn{
            margin-top: 30px;
            height: 60px;

        }
        table{
            margin-top: 30px;
        }
        label{
            color: #0C80AA;
        }
        h1{

            text-align: center;
        }
        a{
            color: #FFFFFF;

        }


       </style>


        </meta>
    </head>
    <body>
        <?php include 'nav/header1.php';?>
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                </div>
                <button class="btn btn-outline-info col-xl-8" data-target=".bd-example-modal-lg" data-toggle="modal" id="btn" type="button">
                    Ingresar
                </button>
                <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content alert-danger" style="filter:alpha(opacity=50); opacity:0.9;">
      <div class="modal-header">
        <h5 class="modal-title">Eliminar</h5>
        <button type="button" style="color: #B54958" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body row">
        <p class="col-lg-7">¿Esta seguro que desea eliminar a :</p>
        <div id="nombre"></div>
        <p>?</p>
        <input type="hidden" id="rutUsuario" name="">
        <p class="col-lg-12">Se eliminaran todo los registros relacionado al usuario</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cerrar</button>
        <button type="button"  class="btn btn-outline-danger btnBorrar1">Confirmar</button>
      </div>
    </div>
  </div>
</div>
                <div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
                            <div class="container">
                                <br>
                                <h4 style="text-align: center; color: #3F78BF">Ingreses los datos del usuario</h4>
                                <br>
                            <form class="form-group" action="ingresarUsuario.php" method="POST">
                                <label>Rut:</label>
                                <input type="text" name="rut" id="rut" class="form-control" onchange="validaRut(this)" required="">
                                <label>Correo:</label>
                                <input type="Email" name="correo" id="correo" class="form-control" required="" >
                                <label>Nombre:</label>
                                <input type="text" name="nombre" id="nombre" class="form-control" required="">
                                <label>Contraseña:</label>
                                <input type="password" name="contrasena" id="contrasena" class="form-control" required="">
                                <label>Repita contraseña:</label>
                                <input type="password" name="contrasena1" id="contrasena1" class="form-control" required="" >
                                <label>Rol:</label>
                                <select class="form-control" name="rol" id="rol" required="">
                                    <option disabled="" selected="">Seleccione un rol</option>
                                    <option value="administrador">administrador</option>
                                    <option value="trabajador">trabajador</option>
                                </select>
                                <label>Sucursal:</label>
                                <select class="form-control" id="codigo" name="codigo" required="">
                    <option disabled="" selected="">Seleccione</option>
                    <?php
include 'conexion.php';
$query = "select * from sucursal";
$datos = mysqli_query($conn, $query);
while ($fila = mysqli_fetch_array($datos)) {
    echo "<option value=" . $fila["idSucursal"] . ">" . $fila["nombreSucursal"] . "</option>";
}
?>
                  </select>
                                <br>
                                <input type="submit" name="" value="Guardar" class="form-control btn-outline-secondary">
                                <br>

                            </form>
                            </div>
                        </div>
                    </div>
                </div>

 <div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade" id="ModalEdita" role="dialog" tabindex="-1">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
                            <div class="container">
                                <br>
                                <h4 style="text-align: center; color: #3F78BF">Actualizar datos</h4>
                                <br>

                                <label>Rut:</label>
                                <input type="text" name="rut1" disabled="" id="rut1" class="form-control">
                                <label>Correo:</label>
                                <input type="Email" name="correo1" id="correo1" class="form-control" >
                                <label>Nombre:</label>
                                <input type="text" name="nombre1" id="nombre1" class="form-control">
                                <label>Contraseña:</label>

                                <input type="password" name="contrasena12" id="contrasena12" class="form-control" >
                                <label>Rol:</label>
                                <select class="form-control" name="rol1" id="rol1">
                                    <option disabled="" selected="">Seleccione un rol</option>
                                    <option value="administrador">administrador</option>
                                    <option value="trabajador">trabajador</option>
                                </select>
                                <label>Sucursal:</label>
                                <select class="form-control" id="codigo1" name="codigo1">
                    <option disabled="" selected="">Seleccione</option>
                    <?php
include 'conexion.php';
$query = "select * from sucursal";
$datos = mysqli_query($conn, $query);
while ($fila = mysqli_fetch_array($datos)) {
    echo "<option value=" . $fila["idSucursal"] . ">" . $fila["nombreSucursal"] . "</option>";

}
?>
                  </select>

                                <br>
                                <button class="btn-outline-info form-control btnActualizar">Actualizar</button>
                                <br>


                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <br>


            <div class="row">
                <div class="col-md-4"></div>
            <?php
if (isset($_GET["fallo"]) && $_GET["fallo"] == 'true') {
    ?>
    <script type="text/javascript">
         toastr.error('Ingrese la misma contraseña');
    </script>
 <?php
}
if (isset($_GET["consulta"]) && $_GET["consulta"] == 'true') {
    ?>
    <script type="text/javascript">
        toastr.success('El usuario ha sido agregado exitosamente');
    </script>

 <?php
}
if (isset($_GET["consulta"]) && $_GET["consulta"] == 'false') {
    ?>
    <script type="text/javascript">
         toastr.error('La insercion fallo, intentelo nuevamente');
    </script>
 <?php
}
if (isset($_GET["eliminar"]) && $_GET["eliminar"] == 'true') {
    ?>
 <script type="text/javascript">
        toastr.success('El usuario ha sido eliminado exitosamente');
    </script>

 <?php
}
if (isset($_GET["eliminar"]) && $_GET["eliminar"] == 'false') {
    ?>

      <script type="text/javascript">
         toastr.error('No se pudo eliminar el usuario');
    </script>

  <?php
}
?>
</div>

    <div id="tablaUsuario" class="container">

    </div>

 </body>
</html>
