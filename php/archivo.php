<?php
session_start();
if ($_SESSION['rol'] != 'admin') {
    header("location: ../index.php?sesion=true");
}
require __DIR__ . '/vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;

$html2pdf = new Html2Pdf('P', 'A4', 'fr');
$html2pdf->writeHTML('<h1 style="text-align: center;">Asistencia organizada por fecha</h1>');
date_default_timezone_set('America/Santiago');
$t               = time();
$fechaINICIO     = date("Y-m-01", $t);
$fechaFIN        = date("Y-m-30", $t);
$fechaComoEntero = strtotime($fechaFIN);
$mes             = date("m", $fechaComoEntero);
$año            = date("Y", $fechaComoEntero);

if ($mes == '01') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 31/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '02') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 28/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '03') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 31/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '04') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 30/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '05') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 31/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '06') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 30/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '07') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 31/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '08') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 31/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '09') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 30/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '10') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 31/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '11') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 30/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
} elseif ($mes == '12') {
    $html2pdf->writeHTML('<h2 style="text-align: center;">01/' . $mes . '/' . $año . ' al 31/' . $mes . "/" . $año . '</h2><br><br><br>');
    $mess = "AsistenciaEnero.pdf";
}
require 'conexion.php';

$Query = "SELECT u.nombreUsuario,a.rutUsuario,a.fecha,a.horaInicio,a.horaSalida,a.horasTrabajadas FROM asistencia a,usuario u where a.rutUsuario=u.rutUsuario AND a.fecha BETWEEN '$año-$mes-01' AND '$año-$mes-31' ORDER BY a.fecha";
$datos = mysqli_query($conn, $Query);
while ($fila = mysqli_fetch_array($datos)) {
    $html2pdf->writeHTML("<b>" . $fila['fecha'] . "</b>  : <p style='color:#404040'>" . $fila['rutUsuario'] . " " . $fila['nombreUsuario'] . "-     Hora de entrada :" . $fila['horaInicio'] . "....Hora de salida:" . $fila['horaSalida'] . "....horas trabajadas  :" . $fila['horasTrabajadas'] . "</p>");
}
$html2pdf->output($mess);
