<?php
session_start();
if ($_SESSION['rol'] != 'trabajador') {
    header("location: ../index.php?sesion=true");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                Empleado
            </title>
            <link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" rel="stylesheet">
            </link>
        </meta>
        <style type="text/css">
            ul{
                background: #FFFDFD;
                padding: 20px;
            }
            ul a{
                font-size: 20px;
            }
            body{
                background: #F1F1F1;
            }
        </style>
    </head>
    <body>
        <header>
            <ul class="nav justify-content-center" id="nav">
                <li class="nav-item">
                    <a class="nav-link active" href="#">
                        Pago
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        Ingresos
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        Gastos
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="../index.php?cerrar=true">
                        Cerrar sesion
                    </a>
                </li>
            </ul>
        </header>
        <script crossorigin="anonymous" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" src="https://code.jquery.com/jquery-3.3.1.slim.min.js">
        </script>
        <script crossorigin="anonymous" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
        </script>
        <script crossorigin="anonymous" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js">
        </script>
    </body>
</html>
