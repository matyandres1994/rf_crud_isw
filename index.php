<?php
session_start();
session_destroy();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                Login
            </title>
            <link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" rel="stylesheet">
                <script src="js/jquery-3.3.1.js">
                </script>
                <script src="js/bootstrap.min.js">
                </script>
                <link href="toast/toastr.min.css" rel="stylesheet" type="text/css">
                    <script src="toast/toastr.min.js" type="text/javascript">
                    </script>


            </link>
            <style type="text/css">
            html{
                font-family:sans-serif;
            }
                #formulario{
                    margin-top: 10%;
                    border-top-style: solid;
                    border-left-style: solid;
                    border-bottom-style: solid;
                    border-right-style: solid;
                    padding: 50px;
                    border-top-color: #18C4C4;
                    border-bottom-color:  #18C4C4;
                    border-right-color: #18C4C4;
                    border-left-color: #18C4C4;
            }

            }
            #textologin{
                color: #083F63;
            }
            #degrade{
                border-color:#0D5988;
            }
            </style>
        </meta>
    </head>
    <body>
        <div class="container">
            <div class="offset-xl-4 col-xl-4" id="formulario">
                <form action="php/validar.php" class="form-group" method="POST" onsubmit="return validar()">
                    <label id="textologin">
                        Rut:
                    </label>
                    <input class="form-control" autocomplete="off" id="usuario" name="usuario" type="text" autocomplete="off" onchange="validaRut(this)" required placeholder="Sin punto ni guion">
                        <label id="textologin">
                            Contraseña:
                        </label>
                        <input class="form-control" required="" autocomplete="off" id="contraseña" name="contraseña" type="password" autocomplete="off"  placeholder="*******">
                            <br>
                                <input class="form-control btn-info" id="" name="" type="submit">
                                </input>
                            </br>
                        </input>
                    </input>
                </form>
                <?php
if (isset($_GET["fallo"]) && $_GET["fallo"] == 'true') {
    ?>
  <script type="text/javascript">
            toastr.error('Datos incorrectos');
        </script>
 <?php
}
if (isset($_GET["sesion"]) && $_GET["sesion"] == 'true') {
    ?>
    <script type="text/javascript">
        toastr.error('Tiene que iniciar sesion antes de acceder');
    </script>
  <?php
}
?>
        </div>
        <script src="js/jslogin.js"></script>
    </body>
</html>
